<?php

namespace QuestionBundle\Controller;

use QuestionBundle\Document\Question;
use QuestionBundle\Document\Tags;
use QuestionBundle\Form\FormQuestionType;
use QuestionBundle\Form\TagsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class TagsController
 * @package QuestionBundle\Controller
 * @Route("/tags", name="tags_main")
 *
 */
class TagsController extends Controller
{
    /**
     * @Route("/new", name="tags_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $tags = new Tags();
        $qa = new Question();
//        $qa->addTag($tags);
        $form = $this->createForm( new FormQuestionType(), $qa );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $dm = $this->get('doctrine.odm.mongodb.document_manager');

            $dm->persist($form);
            $dm->flush();

            $this->redirect(
                $this->generateUrl('tags_show',[
                    'name' => $form->getName()
                ])
            );
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/show", name="tags_show")
     * @Template()
     */
    public function showAction($name)
    {

        return [
            'name' => $name
        ];
    }

}
