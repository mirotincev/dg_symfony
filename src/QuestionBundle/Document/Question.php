<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 27.05.15
 * Time: 15:36
 */

namespace QuestionBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document()
 */
class Question {

    /**
     * @MongoDB\Id(strategy="increment")
     */
    protected $id;

    /**
     * @MongoDB\String()
     */
    protected $title;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Tags", cascade="all")
     */
    protected $tags;



    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add tag
     *
     * @param QuestionBundle\Document\Tags $tag
     */
    public function addTag(\QuestionBundle\Document\Tags $tag)
    {
        $this->tags[] = $tag;
    }

    /**
     * Remove tag
     *
     * @param QuestionBundle\Document\Tags $tag
     */
    public function removeTag(\QuestionBundle\Document\Tags $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection $tags
     */
    public function getTags()
    {
        return $this->tags;
    }
}
