<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 27.05.15
 * Time: 15:42
 */

namespace QuestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use QuestionBundle\Transform\TagsTransformer;

class TagsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title','choice');
    }

    public function setDefault(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuestionBundle\Document\Tags',
        ));
    }

    public function getName()
    {
        return 'tags';
    }

}
