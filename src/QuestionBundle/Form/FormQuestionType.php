<?php

namespace QuestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('tags' ,'document', [
                'class' => 'QuestionBundle\Document\Tags',
                'by_reference' => true,
                'multiple' => 'true',

            ])
            ->add('submit','submit');

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            dump($data);
            dump($data['tags'] );
            dump($form);

            foreach ($data['tags'] as $sub) {
                if(array_key_exists($sub, $data)) {
                    if(is_array($data[$sub])) $$sub = array_combine($data['tags'][$sub], $data['tags'][$sub]);
                } else {
                    $$sub = array();
                }
                $form->add($sub, 'choice', [
                    'label'   => $sub,
                    'choices' => $$sub,
                    'mapped'=>false,
                    'required'=>false,
                    'multiple'=>true,
                ]);
            }

        });

    }



    public function setDefault(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuestionBundle\Document\Question',
        ));
    }

    public function getName()
    {
        return 'question';
    }

}