<?php

namespace Work\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Work\UserBundle\Document\Address;
use Work\UserBundle\Document\Category;
use Work\UserBundle\Document\User;
use Work\UserBundle\Form\AddressType;
use Work\UserBundle\Form\UserType;

class DefaultController extends Controller
{
    /**
     * @Route("/user/{name}/", name="user_index")
     * @Template()
     */
    public function indexAction($name)
	{
		$user = $this->getUserDocument($name);

		return [
			'name' => $name,
			'user' => $user
		];
	}

	/**
	 * @Route("/create/{name}/" , name="user_create")
	 * @Template()
	 */
	public function createAction(Request $request , $name){
		$dm = $this->get('doctrine.odm.mongodb.document_manager');
		$user = $this->getUserDocument($name);

//		$user->addAddress(new Address());

		$formUser = $this->createForm( new UserType() , $user );

		$formUser->handleRequest($request);

		if ($formUser->isValid()) {
			foreach ($user->getAddress() as $address) {
				$address->setSave(0);
				dump($address);
			}

//            $category = new Category();
//            $category->setTitle('юриспрюденция 7');
//            $dm->persist($category);

			$dm->persist($user);
			$dm->flush();
			// ... maybe do some form processing, like saving the Task and Tag objects
		}


		return [
			'formUser' => $formUser->createView(),
			'user' => $user
		];
	}


	public function getUserDocument( $name ){
		$user = $this->get('doctrine.odm.mongodb.document_manager')
			->getRepository('WorkUserBundle:User')
			->findOneBy(['username' => $name]);
		if (!$user)
			throw new NotFoundHttpException('User not found');

		return $user;
	}
}
