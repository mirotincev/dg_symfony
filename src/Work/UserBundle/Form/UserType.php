<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 01.04.15
 * Time: 11:52
 */

namespace Work\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('address' ,'collection', [
				'type' => new AddressType(),
				'allow_add' => true,
				'allow_delete' => true,
				'prototype' => true,
				'by_reference' => true,
				'options' => [
					'data_class' => 'Work\UserBundle\Document\Address'
				]
			])
			->add('submit','submit');

	}

	public function setDefault(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Work\UserBundle\Document\User',
		));
	}

	public function getName()
	{
		return 'user';
	}

}