<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 01.04.15
 * Time: 11:52
 */

namespace Work\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Work\UserBundle\Document\Address;

class AddressType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('city','text')
			->add('street','text')
			->add('home','text');
	}

	public function setDefault(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Work\UserBundle\Document\Address',
		));
	}

	public function getName()
	{
		return 'address';
	}

}