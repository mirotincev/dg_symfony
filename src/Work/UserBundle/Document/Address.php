<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 01.04.15
 * Time: 10:44
 */

namespace Work\UserBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\Document() */
class Address {

	/**
	 * @MongoDB\Id(strategy="increment")
	 */
	protected $id;

	/**
	 * @MongoDB\String()
	 */
	protected $city;

	/**
	 * @MongoDB\String()
	 */
	protected $street;

	/**
	 * @MongoDB\String()
	 */
	protected $home;

	/**
	 * @MongoDB\Bool()
	 */
	protected $save;

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * Get street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set home
     *
     * @param string $home
     * @return self
     */
    public function setHome($home)
    {
        $this->home = $home;
        return $this;
    }

    /**
     * Get home
     *
     * @return string $home
     */
    public function getHome()
    {
        return $this->home;
    }



    /**
     * Set save
     *
     * @param bool $save
     * @return self
     */
    public function setSave($save)
    {
        $this->save = $save;
        return $this;
    }

    /**
     * Get save
     *
     * @return bool $save
     */
    public function getSave()
    {
        return $this->save;
    }
}
