<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 28.04.15
 * Time: 11:14
 */

namespace Work\UserBundle\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


/** @MongoDB\Document() */
class Category {

    /**
     * @MongoDB\Id(strategy="increment")
     */
    protected $id;

    /**
     * @MongoDB\String()
     */
    protected $title;

    /**
     * @MongoDB\String()
     * @Gedmo\Slug(fields={"title"})
     */
    protected $slug;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Category", cascade="all")
     */
    protected $parent;

    /**
    * @MongoDB\Date()
    * @Gedmo\Timestampable(on="create")
    */
    private $created;

    /**
     *
     * @MongoDB\Date()
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @Gedmo\SortablePosition
     * @MongoDB\Integer()
     */
    private $position;


    public function __toString()
    {
        return $this->getTitle();
    }

    public function __construct(){
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug($this->getTitle());
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param Work\UserBundle\Document\Category $parent
     * @return self
     */
    public function setParent(\Work\UserBundle\Document\Category $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return Work\UserBundle\Document\Category $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set created
     *
     * @param date $created
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param date $updated
     * @return self
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get updated
     *
     * @return date $updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }



    /**
     * Set position
     *
     * @param integer $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return integer $position
     */
    public function getPosition()
    {
        return $this->position;
    }
}
