<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 01.04.15
 * Time: 10:28
 */

namespace Work\UserBundle\Document;

use FOS\UserBundle\Document\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="User", repositoryClass="Work\UserBundle\Repository\UserRepository")
  */
class User extends BaseUser{

	/**
	 * @MongoDB\Id(strategy="increment")
	 */
	protected $id;

	/**
	 * @MongoDB\String()
	 */
	protected $name;

	/**
	 * @MongoDB\String()
	 */
	protected $plainPassword;

	/**
	 * @MongoDB\Int
	 */
	protected $level = 0;

	/**
	 * @MongoDB\Float
	 */
	protected $balance = 0;

	/**
	 * @MongoDB\Date
	 */
	protected $createAt;

	/**
	 * @MongoDB\ReferenceMany(targetDocument="Address", cascade="all")
	 */
	protected $address;

    public function __construct()
    {
        parent::__construct();
        $this->address = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     * @return self
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return string $plainPassword
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set level
     *
     * @param int $level
     * @return self
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     *
     * @return int $level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return self
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * Get balance
     *
     * @return float $balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set createAt
     *
     * @param date $createAt
     * @return self
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * Get createAt
     *
     * @return date $createAt
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Add address
     *
     * @param Work\UserBundle\Document\Address $address
     */
    public function addAddress(\Work\UserBundle\Document\Address $address)
    {
        $this->address[] = $address;
    }

    /**
     * Remove address
     *
     * @param Work\UserBundle\Document\Address $address
     */
    public function removeAddress(\Work\UserBundle\Document\Address $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection $address
     */
    public function getAddress()
    {
        return $this->address;
    }
}
