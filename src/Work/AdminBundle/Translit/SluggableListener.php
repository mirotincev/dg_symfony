<?php
/**
 * Created by PhpStorm.
 * User: germani
 * Date: 30.04.15
 * Time: 14:02
 */

namespace Work\AdminBundle\Translit;


class SluggableListener extends \Gedmo\Sluggable\SluggableListener
{

    public function __construct(){
        $this->setTransliterator(array('Work\AdminBundle\Translit\Urlize', 'transliterate'));
    }

    protected function getNamespace()
    {
        return parent::getNamespace();
    }


}