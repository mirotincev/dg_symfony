<?php

namespace Work\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('WorkAdminBundle:Default:index.html.twig', array('name' => $name));
    }
}
